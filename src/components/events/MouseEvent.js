import { createContext, useContext, useEffect, useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import { AppLogic } from './AppLogic';

const MouseContext = createContext();
function MouseEvent({ children }) {
    const valueHandleLogic = useContext(AppLogic);
    const [isMouseDown, setIsMouseDown] = useState(false);
    const [elementSelect, setElementSelect] = useState({});
    const [isMouseMove, setIsMouseMove] = useState(false);
    const [typeElement, setTypeElement] = useState('');
    const [position, setPosition] = useState({});
    const [width, setWidth] = useState(window.innerWidth);

    // set độ rộng cửa sổ
    useEffect(() => {
        const handleResize = () => {
            setWidth(window.innerWidth);
        };

        window.addEventListener('resize', handleResize);
        return () => {
            window.addEventListener('resize', handleResize);
        };
    }, []);

    // sự kiện ấn chuột
    const handleMouseDown = (item, value) => {
        setTypeElement(value);
        setIsMouseDown(true);
        setElementSelect(item);
        setIsMouseMove(false);
    };

    // sự kiện kéo chuột
    const handleMouseMove = (ev) => {
        if (isMouseDown) {
            setIsMouseMove(true);
            setPosition({
                left: ev.clientX,
                top: ev.clientY,
            });

            if (typeElement.type === 'content') {
                const newData = valueHandleLogic.dataMainSection.filter((item, index) => index !== typeElement.ix);
                valueHandleLogic.setDataMainSection(newData);
                setTypeElement({});
            }

            let formData = {
                elememt: elementSelect,
                position: position,
            };
            const newData = valueHandleLogic.dataMainSection.filter(
                (elememt, index) =>
                    elememt.position.left - 32 <= position.left &&
                    position.left <= elememt.position.left + 32 &&
                    elememt.position.top - 32 <= position.top &&
                    position.top <= elememt.position.top + 32,
            );
            valueHandleLogic.setElementIsSelect(formData);
            valueHandleLogic.setElementDuplicate(
                newData[newData.length - 1] !== undefined ? newData[newData.length - 1] : [],
            );
        }
    };

    // sự kiện thả chuột
    const handleMouseUp = () => {
        let newData = {
            idElement: uuidv4(),
            element: elementSelect,
            position: position,
        };
        // nếu vị trí phần mainSection thì thêm elemen vào mảng main || ở sidebar thì xoá
        if (position.left < width - 300 && isMouseMove === true) {
            valueHandleLogic.setDataMainSection([...valueHandleLogic.dataMainSection, newData]);
        }

        //  xử lý khi 2 elemen lồng nhau
        valueHandleLogic.checkRecipes();



        setIsMouseDown(false);
        setElementSelect({});
        setPosition({});
        setIsMouseMove(false);
    };

    const value = {
        isMouseDown,
        setIsMouseDown,
        elementSelect,
        setElementSelect,
        isMouseMove,
        setIsMouseMove,
        typeElement,
        setTypeElement,
        position,
        setPosition,
        handleMouseMove,
        handleMouseDown,
        handleMouseUp,
    };
    return <MouseContext.Provider value={value}>{children}</MouseContext.Provider>;
}

export { MouseContext, MouseEvent };
