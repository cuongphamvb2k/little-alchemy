import { createContext, useEffect, useState } from 'react';
import DataElement from '../Elements/elements';
import { v4 as uuidv4 } from 'uuid';
import dataCombine from './dataCombine';

const AppLogic = createContext();

function HandleLogic({ children }) {
    const [elementDuplicate, setElementDuplicate] = useState([]);
    const [elementIsSelect, setElementIsSelect] = useState({});
    const dataStart = DataElement;

    const [dataMainSection, setDataMainSection] = useState(
        JSON.parse(localStorage.getItem('listElementsMainSection'))
            ? JSON.parse(localStorage.getItem('listElementsMainSection'))
            : [],
    );

    const [dataSideBar, setDataSideBar] = useState(
        JSON.parse(localStorage.getItem('listElementsSideBar'))
            ? JSON.parse(localStorage.getItem('listElementsSideBar'))
            : dataStart.filter((elements) => elements.id < 5),
    );

    
    useEffect(() => {
        localStorage.setItem('listElementsSideBar', JSON.stringify(dataSideBar));
    }, [dataSideBar]);

    
    useEffect(() => {
        localStorage.setItem('listElementsMainSection', JSON.stringify(dataMainSection));
    }, [dataMainSection]);

    function checkRecipes() {
        if (Object.values(elementDuplicate).length > 0 && Object.values(elementIsSelect).length > 0) {
            let a = elementDuplicate.element.title.toLowerCase();
            let b = elementIsSelect.elememt.title.toLowerCase();
            const listNewItem = dataCombine.filter(
                (title) =>
                    (title[0].toLowerCase() === a && title[1].toLowerCase() === b) ||
                    (title[1].toLowerCase() === a && title[0].toLowerCase() === b),
            );

            if (listNewItem.length > 0) {
                let newDataMainSection = [];
                newDataMainSection = dataMainSection.filter(
                    (element) => element.idElement !== elementDuplicate.idElement,
                );
                listNewItem.map((item) => {
                    const createNewItem = dataStart.filter((it) => it.title.toLowerCase() === item[2].toLowerCase());
                    const indexOf = dataSideBar.findIndex(
                        (item) => item.title.toLowerCase() === createNewItem[0].title.toLowerCase(),
                    );
                    if (indexOf === -1) {
                        setDataSideBar([...dataSideBar, createNewItem[0]]);
                    }
                    const formData = {
                        idItemRemove: elementDuplicate.idElement,
                        newCreateItem: {
                            idElement: uuidv4(),
                            element: createNewItem[0],
                            position: elementDuplicate.position,
                        },
                    };

                    newDataMainSection.push(formData.newCreateItem);

                    return null;
                });
                setDataMainSection(newDataMainSection);
                setElementDuplicate([]);
                setElementIsSelect({});
                return true;
            }
        }
        return null;
    }

    const value = {
        dataMainSection,
        setDataMainSection,
        elementDuplicate,
        setElementDuplicate,
        elementIsSelect,
        setElementIsSelect,
        dataSideBar,
        setDataSideBar,
        checkRecipes,
        DataElement,
    };
    return <AppLogic.Provider value={value}> {children}</AppLogic.Provider>;
}

export { AppLogic, HandleLogic };
